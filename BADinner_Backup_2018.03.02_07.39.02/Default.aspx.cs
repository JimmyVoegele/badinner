﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BADinner
{
    public partial class _Default : Page
    {
        public static string myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string sqlQuery = "INSERT INTO DinnerRequests ([Name], [Email], [MobileNumber], [NumberGuests], [Showtime], [SendReminder]) ";
            sqlQuery = sqlQuery + "VALUES (@a, @b, @c, @d, @e, @f)";

            using (SqlConnection connection = new SqlConnection(myConnectionString))
            {

                SqlCommand command = new SqlCommand(sqlQuery, connection);
                command.Parameters.AddWithValue("@a", txtName.Text);
                command.Parameters.AddWithValue("@b", txtEmail.Text);
                command.Parameters.AddWithValue("@c", txtMobile.Text);
                command.Parameters.AddWithValue("@d", ddlNumGuests.Text);
                command.Parameters.AddWithValue("@e", ddlShowTime.Text);
                command.Parameters.AddWithValue("@f", chkNotifyMe.Checked);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            sendconfemail();
        }

        public void sendconfemail()
        {
            string strHead;
            string strBody;
            string strTail;

            // Build the body of the e-mail


                strHead = "";

                strBody = "<p>Thank you for submitting your Ballet Austin Board Member dinner seating request.\n";
                strBody = strBody + "<br />\n";
                strBody = strBody + "<p>You selected a dinner seating for " + ddlNumGuests.Text + " at " + ddlShowTime.Text + ".\n";


            strTail = "";

                sendit(txtEmail.Text, txtName.Text, strHead, strBody, strTail);
        }

        public void sendit(string strEmail, string strName, string sHead, string sBody, string sTail)
        {
            String userName = "toastrequest@stdavidsfoundation.org";
            String password = "bubba";

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);
            MailAddress to = new MailAddress(strEmail);

            mail.From = from;
            mail.To.Add(to);
            //mail.CC.Add("christ.lotz@balletaustin.org");
            //mail.CC.Add("brooke.holmes@balletaustin.org");
            mail.Subject = "TEST: Ballet Austin Dinner Seating Request for " + strName;
            mail.IsBodyHtml = true;
            mail.Body = sHead + sBody + sTail;

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }
    }
}