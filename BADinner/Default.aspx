﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BADinner._Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<%--    <h1>Ballet Austin</h1>--%>
    <table>
        <tr>
            <td>
                <telerik:RadLabel ID="lblName" runat="server" Text="Board Member Name:"></telerik:RadLabel>
            </td>
            <td>
                <telerik:RadTextBox ID="txtName" runat="server" Width="500px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadLabel ID="RadLabel8" runat="server" Text="Email Address:"></telerik:RadLabel>
            </td>
            <td>
                <telerik:RadTextBox ID="txtEmail" runat="server" Width="500px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadLabel ID="RadLabel2" runat="server" Text="Mobile Number:"></telerik:RadLabel>
            </td>
            <td>
                <telerik:RadTextBox ID="txtMobile" runat="server" Width="500px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadLabel ID="RadLabel3" runat="server" Text="Number of Guests:"></telerik:RadLabel>
            </td>
            <td>
                <asp:DropDownList ID="ddlNumGuests" runat="server" Width="500px" >
                    <asp:ListItem>{Please Select}</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadLabel ID="RadLabel4" runat="server" Text="Dinner Seating:"></telerik:RadLabel>
            </td>
            <td>
                <asp:DropDownList ID="ddlShowTime" runat="server" Width="500px">
                    <asp:ListItem>{Please Select}</asp:ListItem>
                    <asp:ListItem>Friday April 6, 2018 @ 5:30 PM</asp:ListItem>
                    <asp:ListItem>Saturday April 7, 2018 @ 5:30 PM</asp:ListItem>
                    <asp:ListItem>Sunday April 8th, 2018 @ 1:00 PM</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <telerik:RadButton ID="btnSubmit" runat="server" Text="Submit Request" OnClick="Submit_Click" SkinID="Office2007"></telerik:RadButton>
    <br />
    <br />
    <asp:CheckBox ID="chkNotifyMe" runat="server" Text="Please send me a text message reminder 24 hours before the show." Checked="true"></asp:CheckBox>
</asp:Content>
