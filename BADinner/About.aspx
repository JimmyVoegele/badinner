﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="BADinner.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h3>Ballet Austin Dinner Request</h3>
    <p>This web application is to provide Ballet Austin Board Members a facility to request a special dinner seating at Austin's Capitol Grille prior to a Ballet Austin performance. Board Members are welcome and encouraged to enjoy these special dinner seatings with other Ballet Austin Board Members who are attending same-night performances.</p>
    <br />
    <p>If there are any questions, please direct them to Christi Lotz or Brooke Holmes.</p>
</asp:Content>
